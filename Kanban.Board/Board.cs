﻿using System.Threading.Tasks;
using Microsoft.ServiceFabric.Actors.Runtime;
using Kanban.Board.Interfaces;

namespace Kanban.Board
{
    [StatePersistence(StatePersistence.Persisted)]
    internal class Board : Actor, IBoard
    {
        Task IBoard.Initialise(string name)
        {
            return this.StateManager.SetStateAsync("Name", name);
        }

        Task<string> IBoard.Details()
        {
            return StateManager.GetStateAsync<string>("Name");            
        }
    }
}
